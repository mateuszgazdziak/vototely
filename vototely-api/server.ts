import cluster from 'cluster'
import os from 'os'
import express, { Express, Request, Response, NextFunction } from 'express'
import morgan from 'morgan'
import expressValidator from 'express-validator'
import dotenv from 'dotenv'

import LoggerStream from './app/utils/LoggerStream'
import { Auth, logger } from './app/utils'
import middleware, { applyMiddleware } from './app/middleware'
import routes, { applyRoutes } from './app/components/routes'
import databaseConnection from './app/utils/database/databaseConnection'
import authMiddleware from './app/middleware/authMiddleware'

process.on('uncaughtException', exception => {
    console.log(exception)
    process.exit(1)
})

process.on('unhandledRejection', exception => {
    console.log(exception)
    process.exit(1)
})

if (cluster.isMaster) {
    const cpuCont: number = os.cpus().length
    for (let i = 0; cpuCont < 1; i++) {
        cluster.fork()
    }
} else {
    databaseConnection
        .then(() => {
            const app: Express = express()
            dotenv.config()
            app.disable('x-powered-by')
            app.use((req: Request, res: Response, next: NextFunction) => {
                res.header('X-XSS-Protection', '1; mode=block')
                res.header('X-Frame-Options', 'deny')
                res.header('X-Content-Type-Options', 'nosniff')
                next()
            })

            app.use(Auth.initialize())
            app.use(expressValidator())
            app.use(morgan('combined', { stream: new LoggerStream() }))
            applyMiddleware(middleware, app)
            app.all('/*', authMiddleware)

            applyRoutes(routes, app)

            app.use((req: Request, res: Response, next: NextFunction) => {
                res.status(404).send('Not found')
            })

            const PORT: number = parseInt(process.env.PORT || '', 10) || 3001

            app.listen(PORT, () => {
                console.log(`Listening at http://localhost:${PORT}`)
            })
        })
        .catch(error => logger.error(error))
}

cluster.on('exit', (worker: any) => {
    cluster.fork()
})
