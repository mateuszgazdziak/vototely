import 'reflect-metadata'
import { createConnection, Connection, ConnectionOptions } from 'typeorm'
import { join } from 'path'
const parentDir = join(__dirname, '../../components')

const connectionOpts: ConnectionOptions = {
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: Number(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'vototely',
    password: process.env.DB_PASSWORD || 'vototely',
    database: process.env.DB_NAME || 'vototely',
    entities: [`${parentDir}/**/*Entity.js`],
    synchronize: true,
    // cache: {
    //     type: 'redis',
    //     duration: 30000,
    //     options: {
    //         host: 'localhost',
    //         port: 6379,
    //     },
    // },
}

const databaseConnection: Promise<Connection> = createConnection(connectionOpts)

export default databaseConnection
