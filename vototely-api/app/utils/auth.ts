import jwt from 'jwt-simple'
import passport from 'passport'
import { Strategy, ExtractJwt } from 'passport-jwt'
import { Request } from 'express'
import moment from 'moment'
import User from '../components/user/userEntity'
import { getRepository } from 'typeorm'

const getStrategy = (): Strategy => {
    const params = {
        secretOrKey: process.env.JWT_SECRET,
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
        passReqToCallback: true,
    }

    return new Strategy(
        params,
        (req: Request, payload: any, callback: Function) => {
            const userRepository = getRepository(User)

            userRepository
                .findOne({ where: { login: payload.login } })
                .then((user: any) => {
                    if (typeof user === 'undefined') {
                        return callback(null, false, 'User not found')
                    }
                    return callback(null, { id: user.id, login: user.login })
                })
                .catch((error: Error) => callback(error))
        }
    )
}

const getToken = (user: User): Object => {
    let expires = moment()
        .utc()
        .add({ days: 1 })
        .unix()
    let token = jwt.encode(
        {
            exp: expires,
            login: user.login,
        },
        String(process.env.JWT_SECRET)
    )

    return {
        token: 'JWT ' + token,
        expires: moment.unix(expires).format(),
        user: user.id,
        isAdmin: user.isAdmin,
    }
}

const initialize = () => {
    passport.use('jwt', getStrategy())
    return passport.initialize()
}

const authenticate = (callback: any) =>
    passport.authenticate(
        'jwt',
        { session: false, failWithError: true },
        callback
    )
export const Auth = { initialize, getToken, authenticate }
