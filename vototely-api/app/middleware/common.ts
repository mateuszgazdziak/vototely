import { Router, Response, NextFunction } from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'

export const asyncMiddleware = (fn: Function) => (
    req: Request,
    res: Response,
    next: NextFunction
) => Promise.resolve(fn(req, res, next)).catch(next)

export const corsMiddleware = (router: Router) => {
    router.use(cors({ credentials: true, origin: true }))
}

export const bodyParserMiddleware = (router: Router) => {
    router.use(bodyParser.urlencoded({ extended: true }))
    router.use(bodyParser.json())
}
