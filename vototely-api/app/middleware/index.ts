import { Router } from 'express'
import { corsMiddleware, bodyParserMiddleware } from './common'

type MiddlewareWrapper = (router: Router) => void

export const applyMiddleware = (
    middlewareWrappers: MiddlewareWrapper[],
    router: Router
) => {
    for (const wrapper of middlewareWrappers) {
        wrapper(router)
    }
}

export default [corsMiddleware, bodyParserMiddleware]
