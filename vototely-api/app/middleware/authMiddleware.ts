import { Request, Response, NextFunction } from 'express'
import { logger, Auth } from './../utils'

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (req.path.includes('login')) {
        return next()
    }

    return Auth.authenticate((error: any, user: any, info: any) => {
        if (error) {
            logger.error(error)
            return next(error)
        }

        if (!user) {
            if (info.name === 'TokenExpiredError') {
                return res.status(401).json({
                    message:
                        'Your token has expired. Please generate a new one',
                })
            } else {
                logger.info(info.message)
                return res.status(401).json({ message: info })
            }
        }
        req.user = user
        return next()
    })(req, res, next)
}

export default authMiddleware
