import {
    Entity,
    Unique,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    AfterInsert,
} from 'typeorm'
import Idea from './../idea/ideaEntity'
@Entity()
@Unique(['login'])
export default class User {
    @PrimaryGeneratedColumn()
    id!: number

    @Column()
    login!: string

    @Column()
    password!: string

    @Column()
    isAdmin!: boolean

    @OneToMany(() => Idea, (idea: Idea) => idea.user)
    ideas!: Idea[]

    @AfterInsert()
    public handleAfterInsert() {
        console.log(`INSERTED USER WITH ID: ${this.id}`)
    }
}
