import { Request, Response } from 'express'
import { getConnection, Repository } from 'typeorm'
import bcrypt from 'bcrypt'
import { AppError, logger } from './../../utils'

import User from './userEntity'
let userRepository: Repository<User>

const initialize = () => {
    const connection = getConnection()
    userRepository = connection.getRepository(User)
}

export const addNewUser = async (req: Request, res: Response) => {
    try {
        if (typeof userRepository === 'undefined') {
            initialize()
        }

        if (!req.user.isAdmin) {
            logger.error(`User ${req.user.login} is not an admin!`)
            throw new AppError(403, AppError.FORBIDDEN)
        }

        req.checkBody('login', 'Invalid username')
            .notEmpty()
            .isLength({ min: 5, max: 64 })
        req.checkBody('password', 'Invalid password')
            .notEmpty()
            .isLength({ min: 5 })

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(400, AppError.BAD_REQUEST)
        }

        const passwordHash = await bcrypt.hash(req.body.password, 10)
        const user = new User()
        user.login = req.body.login
        user.password = passwordHash
        user.isAdmin = false
        await userRepository.save(user)
        res.status(200).json({ message: `User ${user.login} created!` })
    } catch (error) {
        logger.error(error)
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}

export const getAllUsers = async (req: Request, res: Response) => {
    try {
        if (typeof userRepository === 'undefined') {
            initialize()
        }
        const ideas = await userRepository.find()
        res.send({ ideas })
    } catch (error) {
        logger.error(error)
        res.status(403).json({ message: 'Forbidden', errors: error })
    }
}
