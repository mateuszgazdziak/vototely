import { addNewUser, getAllUsers } from './userController'

export default [
    {
        path: '/api/v0/users/new',
        method: 'post',
        handler: [addNewUser],
    },
    {
        path: '/api/v0/users',
        method: 'get',
        handler: [getAllUsers],
    },
]
