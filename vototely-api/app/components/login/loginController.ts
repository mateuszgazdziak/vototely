import { Request, Response } from 'express'
import { AppError, logger, Auth } from './../../utils'
import bcrypt from 'bcrypt'
import { getRepository } from 'typeorm'
import User from './../user/userEntity'

const comparePassword = async (
    reqPassword: string,
    dbPassword: string
): Promise<boolean> => {
    return await bcrypt.compare(reqPassword, dbPassword)
}

const login = async (req: Request, res: Response) => {
    try {
        req.checkBody('login', 'Invalid username').notEmpty()
        req.checkBody('password', 'Invalid password').notEmpty()

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(401, AppError.MISSING_CREDENTIALS)
        }

        const user = await getRepository(User).findOne({
            where: { login: req.body.login },
        })

        if (!user) {
            logger.info('User does not exist')
            throw new AppError(401, AppError.USER_NOT_FOUND)
        }

        const isPasswordValid = await comparePassword(
            req.body.password,
            user.password
        )

        if (!isPasswordValid) {
            throw new AppError(401, AppError.MISSING_CREDENTIALS)
        }

        res.status(200).json(Auth.getToken(user))
    } catch (error) {
        logger.error(`Invalid credentials ${error}`)
        res.status(401).json({ message: 'Invalid credentials', errors: error })
    }
}

export { login }
