import { login } from './loginController'

export default [
    {
        path: '/login',
        method: 'post',
        handler: [login],
    },
]
