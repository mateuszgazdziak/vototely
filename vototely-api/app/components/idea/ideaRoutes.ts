import {
    getAllIdeas,
    addNewIdea,
    editIdea,
    voteIdea,
    deleteIdea,
} from './ideaController'

export default [
    {
        path: '/api/v0/ideas',
        method: 'get',
        handler: [getAllIdeas],
    },
    {
        path: '/api/v0/ideas/new',
        method: 'post',
        handler: [addNewIdea],
    },
    {
        path: '/api/v0/ideas/edit',
        method: 'put',
        handler: [editIdea],
    },
    {
        path: '/api/v0/ideas/vote',
        method: 'put',
        handler: [voteIdea],
    },
    {
        path: '/api/v0/ideas/delete',
        method: 'delete',
        handler: [deleteIdea],
    },
]
