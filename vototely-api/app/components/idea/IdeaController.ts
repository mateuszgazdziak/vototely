import { Request, Response } from 'express'
import { getConnection, Repository } from 'typeorm'
import { AppError, logger } from './../../utils'

import Idea from './ideaEntity'
let ideaRepository: Repository<Idea>

const initialize = () => {
    const connection = getConnection()
    ideaRepository = connection.getRepository(Idea)
}

export const getAllIdeas = async (req: Request, res: Response) => {
    try {
        if (typeof ideaRepository === 'undefined') {
            initialize()
        }
        const ideas = await ideaRepository.find({
            cache: true,
            where: { isDeleted: false },
        })
        res.send({ ideas })
    } catch (error) {
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}

export const addNewIdea = async (req: Request, res: Response) => {
    try {
        req.checkBody('description', 'Wrong description')
            .notEmpty()
            .isLength({ min: 5, max: 512 })

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(400, AppError.BAD_REQUEST)
        }

        const idea = new Idea()
        idea.description = req.body.description
        idea.user = req.user.id

        if (typeof ideaRepository === 'undefined') {
            initialize()
        }

        await ideaRepository.save(idea)
        res.status(200).json({ message: `New idea added` })
    } catch (error) {
        logger.error(error)
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}

export const editIdea = async (req: Request, res: Response) => {
    try {
        req.checkBody('ideaId', 'Not such idea').notEmpty().isNumeric
        req.checkBody('description', 'Wrong description')
            .notEmpty()
            .isLength({ min: 5, max: 512 })

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(400, AppError.BAD_REQUEST)
        }

        if (typeof ideaRepository === 'undefined') {
            initialize()
        }

        const idea = await ideaRepository.findOne({
            cache: true,
            relations: ['user'],
            where: { id: req.body.ideaId },
        })

        if (!idea) {
            logger.error(`Idea with ${req.body.id} does not exist!`)
            throw new AppError(401, AppError.IDEA_NOT_FOUND)
        }

        if (req.body.description && idea.user.id !== req.user.id) {
            logger.error(
                `User ${
                    req.user.login
                } cannot edit idea created by another user`
            )
            throw new AppError(403, AppError.FORBIDDEN)
        }

        idea.description = req.body.description

        await ideaRepository.save(idea)
        res.status(200).json({ message: `Idea edited` })
    } catch (error) {
        logger.error(error)
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}

export const voteIdea = async (req: Request, res: Response) => {
    try {
        req.checkBody('ideaId', 'Not such idea').notEmpty().isNumeric
        req.checkBody('voteUp', 'Forbidden vote').toBoolean().isBoolean
        req.checkBody('voteDown', 'Forbidden vote').toBoolean().isBoolean

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(400, AppError.BAD_REQUEST)
        }

        if (typeof ideaRepository === 'undefined') {
            initialize()
        }

        const idea = await ideaRepository.findOne({
            cache: true,
            where: { id: req.body.ideaId },
        })

        if (!idea) {
            logger.error(`Idea with ${req.body.ideaId} does not exist!`)
            throw new AppError(403, AppError.IDEA_NOT_FOUND)
        }

        if (req.body.voteDown) idea.votesDown++
        if (req.body.voteUp) idea.votesUp++

        await ideaRepository.save(idea)
        res.status(200).json({ message: `Vote added` })
    } catch (error) {
        logger.error(error)
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}

export const deleteIdea = async (req: Request, res: Response) => {
    try {
        req.checkBody('ideaId', 'Wrong description').notEmpty().isNumeric

        let errors = req.validationErrors()
        if (errors) {
            logger.error(errors)
            throw new AppError(400, AppError.BAD_REQUEST)
        }

        if (typeof ideaRepository === 'undefined') {
            initialize()
        }

        const idea = await ideaRepository.findOne({
            cache: true,
            relations: ['user'],
            where: { id: req.body.ideaId },
        })

        if (!idea) {
            logger.error(`Idea with ${req.body.ideaId} does not exist!`)
            throw new AppError(403, AppError.IDEA_NOT_FOUND)
        }

        if (idea.user.id !== req.user.id) {
            logger.error(
                `User ${
                    req.user.login
                } cannot delete idea created by another user`
            )
            throw new AppError(403, AppError.FORBIDDEN)
        }

        idea.isDeleted = true
        await ideaRepository.save(idea)
        res.status(200).json({ message: `Idea deleted` })
    } catch (error) {
        logger.error(error)
        const errorCode = error.status || 403
        const errorMessage = error.message || 'Forbidden'
        res.status(errorCode).json({ message: errorMessage, errors: error })
    }
}
