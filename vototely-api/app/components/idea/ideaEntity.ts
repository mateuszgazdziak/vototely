import {
    AfterInsert,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    Index,
    JoinColumn,
} from 'typeorm'
import User from './../user/userEntity'
import { Length } from 'class-validator'

@Entity()
export default class Idea {
    @PrimaryGeneratedColumn()
    id!: number

    @Column('text')
    @Length(5, 512)
    description!: string

    @Column({ default: 0 })
    votesUp!: number

    @Column({ default: 0 })
    votesDown!: number

    @Index()
    @ManyToOne(type => User, (user: User) => user.ideas)
    @JoinColumn()
    user!: User

    @AfterInsert()
    public handleAfterInsert() {
        console.log(`INSERTED IDEA WITH ID: ${this.id}`)
    }

    @Column({ default: false })
    isDeleted!: boolean
}
