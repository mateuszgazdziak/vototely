import { Router, Request, Response, NextFunction } from 'express'
import ideaRoutes from './idea/ideaRoutes'
import userRoutes from './user/userRoutes'
import loginRoutes from './login/loginRoutes'

type RouteHandler = (
    req: Request,
    res: Response,
    next: NextFunction
) => Promise<void> | void

type Route = {
    path: string
    method: string
    handler: RouteHandler | RouteHandler[]
}

export default [...ideaRoutes, ...userRoutes, ...loginRoutes]

export const applyRoutes = (routes: Route[], router: Router) => {
    for (const route of routes) {
        const { method, path, handler } = route
        ;(router as any)[method](path, handler)
    }
}
