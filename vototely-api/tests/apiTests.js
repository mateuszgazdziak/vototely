const request = require('supertest')

const userCredentials = {
    login: 'test',
    password: 'test',
}

const user = request.agent('http://127.0.0.1:3001')
let JWT_TOKEN

before(async () => {
    const result = await user.post('/login').send(userCredentials)
    JWT_TOKEN = result.body.token
})

// REQUESTS WITHOUT TOKEN

describe('GET /api/v0/users', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .get('/api/v0/users')
            .expect(401, done)
    })
})

describe('GET /api/v0/ideas', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .get('/api/v0/ideas')
            .expect(401, done)
    })
})

describe('POST /api/v0/ideas/new', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .post('/api/v0/ideas/new')
            .expect(401, done)
    })
})

describe('PUT /api/v0/ideas/edit', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .put('/api/v0/ideas/edit')
            .expect(401, done)
    })
})

describe('PUT /api/v0/ideas/vote', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .put('/api/v0/ideas/vote')
            .expect(401, done)
    })
})

describe('DELETE /api/v0/ideas/delete', () => {
    it('should return 401 if request does not contain jwt token', done => {
        request('http://127.0.0.1:3001')
            .delete('/api/v0/ideas/delete')
            .expect(401, done)
    })
})

// USERS

describe('POST /api/v0/users/new', () => {
    it('should return 403 if logged user is not an admin', done => {
        user.post('/api/v0/users/new')
            .set('Authorization', JWT_TOKEN)
            .send({ login: 'fff', password: 'dddd' })
            .expect(403, done)
    })
})

describe('POST /api/v0/users', () => {
    it('should return 200 ', done => {
        user.get('/api/v0/users')
            .set('Authorization', JWT_TOKEN)
            .expect(200, done)
    })
})

// IDEAS

describe('GET /api/v0/ideas', () => {
    it('should return 200 user is logged', done => {
        user.get('/api/v0/ideas')
            .set('Authorization', JWT_TOKEN)
            .expect(200, done)
    })
})

describe('POST /api/v0/ideas', () => {
    it('should return 400 for description length below 5', done => {
        user.post('/api/v0/ideas/new')
            .set('Authorization', JWT_TOKEN)
            .send({ description: 'test' })
            .expect(400, done)
    })
})

describe('POST /api/v0/ideas', () => {
    it('should return 400 for description length above 512', done => {
        user.post('/api/v0/ideas/new')
            .set('Authorization', JWT_TOKEN)
            .send({
                description: `K1gOyu5ilIiXKmbEoT8gl9q5E3cf25HkissFxZh9xDxLOUIUxmbgMIW3saWsw8VGZji610WPy4lbOTDi2lbIsCk5sr7
                Py6H0rfEDtoWiq7nfnwlFatO6tzsgimuj1evMoiUE92669Hw9QJHccXZzNiUiH6aRqKyKUaDDIbl7apKaN2bi1PSuzOLLAsHpX6UGxQttOI
                LIHRK3O01xOBzwxysvrV0LZgDmejQp3MVjUJ1CQx7fmG4pX9seVt58HxjLfx6EXXgTSAjWgMHO6hY8p1l2JaWnq4cd3CWlhq4mZQRxGgWE
                vGBZqFqGpjuC9urV2qn7YQOc6hQEsb47QHIuVlDAXwTyIGOzEzPyoGpzPk0HiRTkb28CPrFO7gHrdbTRYah4KtBmpGfFXL3D4URaFtFkU4
                PbfzgG8aKOUUyWsI7iQPS41jILaw1JTLQt8KKrFliTJoiMSkQtkP5HJZg2b294K13i3vebEl1F0IvgLcYB8O7pWxlp2YdJCmporrcNt`,
            })
            .expect(400, done)
    })
})

describe('POST /api/v0/ideas', () => {
    it('should return 200 for proper description', done => {
        user.post('/api/v0/ideas/new')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
                description: `Description`,
            })
            .expect(200, done)
    })
})

describe('PUT /api/v0/ideas', () => {
    it('should return 400 for request without idea ID', done => {
        user.put('/api/v0/ideas/edit')
            .set('Authorization', JWT_TOKEN)
            .send({
                description: 'description',
            })
            .expect(400, done)
    })
})

describe('PUT /api/v0/ideas', () => {
    it('should return 400 for description length below 5', done => {
        user.put('/api/v0/ideas/edit')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
                description: 'test',
            })
            .expect(400, done)
    })
})

describe('PUT /api/v0/ideas', () => {
    it('should return 400 for description length above 512', done => {
        user.put('/api/v0/ideas/edit')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
                description: `K1gOyu5ilIiXKmbEoT8gl9q5E3cf25HkissFxZh9xDxLOUIUxmbgMIW3saWsw8VGZji610WPy4lbOTDi2lbIsCk5sr7
                Py6H0rfEDtoWiq7nfnwlFatO6tzsgimuj1evMoiUE92669Hw9QJHccXZzNiUiH6aRqKyKUaDDIbl7apKaN2bi1PSuzOLLAsHpX6UGxQttOI
                LIHRK3O01xOBzwxysvrV0LZgDmejQp3MVjUJ1CQx7fmG4pX9seVt58HxjLfx6EXXgTSAjWgMHO6hY8p1l2JaWnq4cd3CWlhq4mZQRxGgWE
                vGBZqFqGpjuC9urV2qn7YQOc6hQEsb47QHIuVlDAXwTyIGOzEzPyoGpzPk0HiRTkb28CPrFO7gHrdbTRYah4KtBmpGfFXL3D4URaFtFkU4
                PbfzgG8aKOUUyWsI7iQPS41jILaw1JTLQt8KKrFliTJoiMSkQtkP5HJZg2b294K13i3vebEl1F0IvgLcYB8O7pWxlp2YdJCmporrcNt`,
            })
            .expect(400, done)
    })
})

describe('PUT /api/v0/ideas', () => {
    it('should return 200 for proper description', done => {
        user.put('/api/v0/ideas/edit')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 2,
                description: `Description`,
            })
            .expect(200, done)
    })
})

describe('PUT /api/v0/vote', () => {
    it('should return 200 for vote up', done => {
        user.put('/api/v0/ideas/vote')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
                voteUp: true,
                voteDown: false,
            })
            .expect(200, done)
    })
})

describe('PUT /api/v0/vote', () => {
    it('should return 200 for vote down', done => {
        user.put('/api/v0/ideas/vote')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
                voteDown: true,
                voteUp: false,
            })
            .expect(200, done)
    })
})

describe('PUT /api/v0/vote', () => {
    it('should return 400 for wrong params', done => {
        user.put('/api/v0/ideas/vote')
            .set('Authorization', JWT_TOKEN)
            .send({
                voteDown: true,
                voteUp: false,
            })
            .expect(400, done)
    })
})

describe('PUT /api/v0/vote', () => {
    it('should return 403 if idea does not exist', done => {
        user.put('/api/v0/ideas/vote')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 77,
            })
            .expect(403, done)
    })
})

describe('PUT /api/v0/delete', () => {
    it('should return 400 for wrong params', done => {
        user.delete('/api/v0/ideas/delete')
            .set('Authorization', JWT_TOKEN)
            .send({})
            .expect(400, done)
    })
})

describe('PUT /api/v0/delete', () => {
    it('should delete idea if user is an author', done => {
        user.delete('/api/v0/ideas/delete')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 1,
            })
            .expect(200, done)
    })
})

describe('PUT /api/v0/delete', () => {
    it('should return 403 if idea does not exist', done => {
        user.delete('/api/v0/ideas/delete')
            .set('Authorization', JWT_TOKEN)
            .send({
                ideaId: 77,
            })
            .expect(403, done)
    })
})
