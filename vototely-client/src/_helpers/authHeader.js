const authHeader = () => {
  let token = sessionStorage.getItem("jwtToken");

  if (token) {
    return { Authorization: token };
  } else {
    return {};
  }
};

export default authHeader;
