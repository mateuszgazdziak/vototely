import { combineReducers } from "redux";
import user from "./user/reducer";
import idea from "./idea/reducer";

const rootReducer = combineReducers({ user, idea });

export default rootReducer;
