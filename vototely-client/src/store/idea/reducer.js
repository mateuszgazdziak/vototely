import { actionTypes } from "./actions";

const getInitialState = () => ({
  ideas: [],
  isFetching: false,
  error: ""
});

const ideas = (state = getInitialState(), { type, payload }) => {
  switch (type) {
    case actionTypes.FETCH_IDEAS_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case actionTypes.FETCH_IDEAS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        ideas: [...state.ideas, ...payload.ideas]
      };
    case actionTypes.FETCH_IDEAS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: payload.error
      };
    default:
      return state;
  }
};

export default ideas;
