import vototelyApi from "./../../services/vototelyApi";

const NS = "@vototely/idea";

export const actionTypes = {
  FETCH_IDEAS_REQUEST: `${NS}/FETCH_IDEAS_REQUEST`,
  FETCH_IDEAS_SUCCESS: `${NS}/FETCH_IDEAS_SUCCESS`,
  FETCH_IDEAS_FAILURE: `${NS}/FETCH_IDEAS_FAILURE`,
  ADD_IDEA_SUCCESS: `${NS}/ADD_IDEA_SUCCESS`,
  ADD_IDEA_REQUEST: `${NS}/ADD_IDEA_REQUEST`,
  ADD_IDEA_FAILURE: `${NS}/ADD_IDEA_FAILURE`,
  EDIT_IDEA_SUCCESS: `${NS}/EDIT_IDEA_SUCCESS`,
  EDIT_IDEA_REQUEST: `${NS}/EDIT_IDEA_REQUEST`,
  EDIT_IDEA_FAILURE: `${NS}/EDIT_IDEA_FAILURE`,
  VOTE_IDEA_SUCCESS: `${NS}/VOTE_IDEA_SUCCESS`,
  VOTE_IDEA_REQUEST: `${NS}/VOTE_IDEA_REQUEST`,
  VOTE_IDEA_FAILURE: `${NS}/VOTE_IDEA_FAILURE`
};

const action = (type, payload) => ({ type, payload });

const actions = {
  fetchIdeas: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.FETCH_IDEAS_REQUEST), payload);

      return vototelyApi
        .getIAllIdeas()
        .then(ideas => {
          dispatch(action(actionTypes.FETCH_IDEAS_SUCCESS, ideas));
        })
        .catch(error =>
          dispatch(action(actionTypes.FETCH_IDEAS_FAILURE, error))
        );
    };
  },
  addIdea: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.ADD_IDEA_REQUEST), payload);
      return vototelyApi
        .add({ description: payload })
        .then(() => dispatch(action(actionTypes.ADD_IDEA_SUCCESS), payload))
        .catch(error => dispatch(action(actionTypes.ADD_IDEA_FAILURE), error));
    };
  },
  editIdea: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.EDIT_IDEA_REQUEST), payload);
      return vototelyApi
        .edit(payload)
        .then(() => dispatch(action(actionTypes.EDIT_IDEA_SUCCESS), payload))
        .catch(error => dispatch(action(actionTypes.EDIT_IDEA_FAILURE), error));
    };
  },
  voteIdea: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.VOTE_IDEA_REQUEST), payload);
      return vototelyApi
        .vote(payload)
        .then(() => dispatch(action(actionTypes.VOTE_IDEA_SUCCESS), payload))
        .catch(error => dispatch(action(actionTypes.VOTE_IDEA_FAILURE), error));
    };
  },

  deleteIdea: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.EDIT_IDEA_REQUEST), payload);
      return vototelyApi
        .delete(payload)
        .then(() => dispatch(action(actionTypes.EDIT_IDEA_SUCCESS), payload))
        .catch(error => dispatch(action(actionTypes.EDIT_IDEA_FAILURE), error));
    };
  }
};

export default actions;
