import { actionTypes } from "./actions";

const getInitialState = () => ({
  user: {},
  isFetching: false,
  error: {}
});

const user = (state = getInitialState(), { type, payload }) => {
  switch (type) {
    case actionTypes.FETCH_LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case actionTypes.FETCH_LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        user: payload.user
      };
    case actionTypes.FETCH_LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: payload.error
      };
    default:
      return state;
  }
};

export default user;
