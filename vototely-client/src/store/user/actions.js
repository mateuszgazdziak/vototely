import vototelyApi from "./../../services/vototelyApi";

const NS = "@vototely/idea";

export const actionTypes = {
  FETCH_LOGIN_REQUEST: `${NS}/FETCH_LOGIN_REQUEST`,
  FETCH_LOGIN_SUCCESS: `${NS}/FETCH_LOGIN_SUCCESS`,
  FETCH_LOGIN_FAILURE: `${NS}/FETCH_LOGIN_FAILURE`
};

const action = (type, payload) => ({ type, payload });

const actions = {
  login: (payload = {}) => {
    return dispatch => {
      dispatch(action(actionTypes.FETCH_LOGIN_REQUEST), payload);

      return vototelyApi
        .login({ ...payload })
        .then(user => {
          dispatch(action(actionTypes.FETCH_LOGIN_SUCCESS, { user }));
          sessionStorage.setItem("jwtToken", user.token);
        })
        .catch(error => {
          dispatch(action(actionTypes.FETCH_LOGIN_FAILURE, { error }));
        });
    };
  }
};

export default actions;
