import ApiService, { SERVER_ROOT, API_ROOT } from "./api";
import authHeader from "./../_helpers/authHeader";
const BASE_URL = "https://localhost";

const headers = authHeader();

const client = new ApiService({ baseURL: BASE_URL, headers });

const vototelyApi = {};

vototelyApi.login = ({ login, password }) =>
  client.post(`${SERVER_ROOT}/login`, { login, password });

vototelyApi.getIAllIdeas = () => client.get(`${API_ROOT}/ideas`);

vototelyApi.add = ({ description }) =>
  client.post(`${API_ROOT}/ideas/new`, { description });

vototelyApi.edit = ({ ideaId, description }) =>
  client.put(`${API_ROOT}/ideas/edit`, { ideaId, description });

vototelyApi.vote = ({ ideaId, voteUp, voteDown }) =>
  client.put(`${API_ROOT}/ideas/vote`, { ideaId, voteUp, voteDown });

vototelyApi.delete = ({ ideaId }) =>
  client.post(`${API_ROOT}/ideas/delete`, { ideaId });

export default vototelyApi;
