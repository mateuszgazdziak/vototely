import { connect } from "react-redux";
import NewIdea from "./NewIdea";
import actions from "../../store/idea/actions";

const mapStateToProps = state => ({
  ideas: state.ideas
});

const mapDispatchToProps = dispatch => {
  return {
    addIdea: description => dispatch(actions.addIdea(description))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewIdea);
