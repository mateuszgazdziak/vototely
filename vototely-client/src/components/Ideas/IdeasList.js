import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Label from "@material-ui/icons/Label";
import ThumbDown from "@material-ui/icons/ThumbDown";
import ThumbUp from "@material-ui/icons/ThumbUp";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { Route } from "react-router-dom";
import { withRouter } from "react-router-dom";

const styles = theme => ({
  root: {
    flexGrow: 1,
    maxWidth: 752
  },
  demo: {
    backgroundColor: theme.palette.background.paper
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
  }
});

class IdeasList extends React.Component {
  constructor(props) {
    super(props);
    this.handleSave = this.handleSave.bind(this);
  }
  state = {
    dense: false,
    secondary: false
  };

  componentWillMount() {
    this.props.fetchIdeas();
  }

  handleSave() {
    this.props.history.push("/ideas/new");
  }

  handleVote(args) {
    this.props.handleVote(args);
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={16}>
          <Grid item xs={12} md={6}>
            <Typography variant="h4" className={classes.title}>
              List of Ideas
            </Typography>
            <div className={classes.demo}>
              <List>
                {this.props.ideas.map(idea => (
                  <ListItem key={idea.id}>
                    <ListItemAvatar>
                      <Avatar>
                        <Label />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={`${idea.description}`} />
                    <ListItemSecondaryAction>
                      <IconButton
                        onClick={() =>
                          this.handleVote({
                            ideaId: idea.id,
                            voteUp: true,
                            voteDown: false
                          })
                        }
                        aria-label="ThumbUp"
                      >
                        <ThumbUp />
                      </IconButton>
                      {idea.voteUp}
                      <IconButton
                        onClick={() =>
                          this.handleVote({
                            ideaId: idea.id,
                            voteUp: false,
                            voteDown: true
                          })
                        }
                        aria-label="ThumbDown"
                      >
                        <ThumbDown />
                        {idea.voteDown}
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            </div>
          </Grid>
        </Grid>
        <Route>
          <Fab
            color="primary"
            aria-label="Add"
            className={classes.fab}
            onClick={this.handleSave}
          >
            <AddIcon />
          </Fab>
        </Route>
      </div>
    );
  }
}

IdeasList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(IdeasList));
