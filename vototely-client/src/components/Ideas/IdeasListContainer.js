import { connect } from "react-redux";
import IdeasList from "./IdeasList";
import actions from "../../store/idea/actions";

const mapStateToProps = state => ({
  ideas: state.idea.ideas
});

const mapDispatchToProps = dispatch => {
  return {
    fetchIdeas: () => dispatch(actions.fetchIdeas()),
    handleVote: ({ ideaId, voteUp, voteDown }) =>
      dispatch(actions.voteIdea({ ideaId, voteUp, voteDown }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeasList);
