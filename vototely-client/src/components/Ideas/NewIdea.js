import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

class NewIdea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: ""
    };
    this.handleSave = this.handleSave.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);
  }

  onDescriptionChange(event) {
    this.setState({ description: event.target.value });
  }

  handleSave() {
    this.props.addIdea(this.state.description);
  }
  handleCancel() {
    this.props.history.push("/ideas");
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <TextField
          id="standard-full-width"
          label="New Idea"
          style={{ margin: 8 }}
          placeholder="Enter idea"
          fullWidth
          onChange={this.onDescriptionChange}
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
        />
        <Button
          variant="outlined"
          size="large"
          color="primary"
          className={classes.margin}
          onClick={this.handleCancel}
        >
          CANCEL
        </Button>
        <Button
          variant="contained"
          size="large"
          color="primary"
          className={classes.margin}
          onClick={this.handleSave}
        >
          SAVE
        </Button>
      </div>
    );
  }
}

NewIdea.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles()(NewIdea));
