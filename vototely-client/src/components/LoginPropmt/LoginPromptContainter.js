import { connect } from "react-redux";
import LoginPrompt from "./LoginPropmt";
import actions from "./../../store/user/actions";

const mapStateToProps = (state, onwProps) => ({
  user: state.user,
  error: state.error
});

const mapDispatchToProps = dispatch => {
  return {
    login: ({ login, password }) => dispatch(actions.login({ login, password }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPrompt);
