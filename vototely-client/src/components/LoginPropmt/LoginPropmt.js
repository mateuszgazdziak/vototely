import React, { Component } from "react";
import {
  Paper,
  withStyles,
  Grid,
  TextField,
  Button,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import { withRouter } from "react-router-dom";

const styles = theme => ({
  margin: {
    margin: theme.spacing.unit * 2
  },
  padding: {
    padding: theme.spacing.unit
  }
});

class LoginPrompt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: ""
    };

    this.onLoginChange = this.onLoginChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  onLoginChange(event) {
    this.setState({ login: event.target.value });
  }

  onPasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleLogin() {
    this.props
      .login({
        login: this.state.login,
        password: this.state.password
      })
      .then(() => this.props.history.push("./ideas"));
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.padding}>
        <div className={classes.margin}>
          <Grid container spacing={8} alignItems="flex-end">
            <Grid item md={true} sm={true} xs={true}>
              <TextField
                id="login"
                label="login"
                type="email"
                autoFocus
                onBlur={this.onLoginChange}
                required
              />
            </Grid>
          </Grid>
          <Grid container spacing={8} alignItems="flex-end">
            <Grid item md={true} sm={true} xs={true}>
              <TextField
                id="password"
                label="Password"
                type="password"
                onBlur={this.onPasswordChange}
                required
              />
            </Grid>
          </Grid>
          <Grid container alignItems="center" justify="space-between" />
          <Grid container justify="center" style={{ marginTop: "10px" }}>
            <Button
              variant="outlined"
              color="primary"
              style={{ textTransform: "none" }}
              onClick={this.handleLogin}
            >
              Login
            </Button>
          </Grid>
        </div>
      </Paper>
    );
  }
}

export default withRouter(withStyles(styles)(LoginPrompt));
