import React, { Component } from "react";
import "./App.css";
import AppBar from "./components/AppBar/AppBar";
import NewIdea from "./components/Ideas/NewIdeaContainer";
import LoginPrompt from "./components/LoginPropmt/LoginPromptContainter";
import IdeasList from "./components/Ideas/IdeasListContainer";
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect
} from "react-router-dom";

const Ideas = () => (
  <div>
    <h2>Ideas</h2>
  </div>
);

const About = () => (
  <div>
    <h2>About</h2>
  </div>
);

const fakeAuth = {
  isAuthenticated: false,
  authenticate(cb) {
    this.isAuthenticated = true;
    setTimeout(cb, 100);
  },
  signout(cb) {
    this.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      fakeAuth.isAuthenticated === true ? (
        <Component {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <AppBar />
          <Switch>
            <Route exact path="/" component={Ideas} />
            <Route exact path="/about" component={About} />
            <Route exact path="/login" component={LoginPrompt} />
            <Route exact path="/ideas/new" component={NewIdea} />
            <Route exact path="/ideas" component={IdeasList} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
